I. Tools:
All files can be found on bitbuck
https://bitbucket.org/stockfish/machine_learning-a1/src/master/

This file contains the instructions for how to run the code for AssignmentTo run the code please use Anaconda and Jupiter notebook (Python 2.7).

The 2 clustering algorithms and 4 dimensionality reduction algorithms are all using scikit-learn library.

II. Datasets:
The datasets used for analysis are from UCI Machine Learning Repository:
Dataset_1: Breast Cancer. (Breast Cancer Wisconsin (Diagnostic) Data Set).
breast-cancer-w-diag.csv
http://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+%28diagnostic%29

Dataset_2. StarCraft (SkillCraft1 Master Table Dataset)
starcraft3_nomissing.csv
http://archive.ics.uci.edu/ml/datasets/skillcraft1+master+table+dataset

