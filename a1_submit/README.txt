submission files can be found on bitbucket
https://bitbucket.org/stockfish/machine_learning-a1/src/master/

I. Tools:
Jupiter notebook (Python 2.7).

II. Datasets:
The datasets used for analysis are from UCI Machine Learning Repository:
Dataset_1: Breast Cancer. (Breast Cancer Wisconsin (Diagnostic) Data Set).
http://archive.ics.uci.edu/ml/datasets/breast+cancer+wisconsin+%28diagnostic%29

Dataset_2. StarCraft (SkillCraft1 Master Table Dataset)
http://archive.ics.uci.edu/ml/datasets/skillcraft1+master+table+dataset

III. Code file:
1.  cancer_submit.ipynb
2.  starcraft3_submit.ipynb
