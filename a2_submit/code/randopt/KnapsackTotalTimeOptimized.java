package randopt;

import java.util.Arrays;
import java.util.Random;

import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;

import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.UniformCrossOver;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * Knapsack Problem: calculate total time for optimization of RHC, SA, GA and MIMIC
 * using best parameters defined.
 * 
 * Modified by Xiaolu, original version is Andrew Guillory's CountOnesTest
 */
public class KnapsackTotalTimeOptimized {
	/** Random number generator */
	private static final Random random = new Random();
	/** The number of items */
	private static final int NUM_ITEMS = 40;
	/** The number of copies each */
	private static final int COPIES_EACH = 1;
	/** The maximum value for a single element */
	private static final double MAX_VALUE = 100;
	/** The maximum weight for a single element */
	private static final double MAX_WEIGHT = 100;
	/** The maximum weight for the knapsack */
	private static final double MAX_KNAPSACK_WEIGHT = MAX_WEIGHT * NUM_ITEMS * COPIES_EACH * .1;

	/**
	 * The test main
	 * 
	 * @param args
	 *            ignored
	 */
	public static void main(String[] args) {
		int[] copies = new int[NUM_ITEMS];
		Arrays.fill(copies, COPIES_EACH);
		double[] values = new double[NUM_ITEMS];
		double[] weights = new double[NUM_ITEMS];
		for (int i = 0; i < NUM_ITEMS; i++) {
			values[i] = random.nextDouble() * MAX_VALUE;
			weights[i] = random.nextDouble() * MAX_WEIGHT;
		}
		int[] ranges = new int[NUM_ITEMS];
		Arrays.fill(ranges, COPIES_EACH + 1);

		EvaluationFunction ef = new KnapsackEvaluationFunction(values, weights, MAX_KNAPSACK_WEIGHT, copies);
		Distribution odd = new DiscreteUniformDistribution(ranges);
		NeighborFunction nf = new DiscreteChangeOneNeighbor(ranges);

		MutationFunction mf = new DiscreteChangeOneMutation(ranges);
		CrossoverFunction cf = new UniformCrossOver();
		Distribution df = new DiscreteDependencyTree(.1, ranges);

		HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
		GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
		ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

		int repeat = 100;
		// the optimized iteration numbers from the "KnapsackTest"
		int itr_rhc = 150;//200
		int itr_sa = 180;
		int itr_ga = 200; //250
		int itr_mimic = 20; //50

		double[] result_rhc = new double[repeat];
		double[] result_sa = new double[repeat];
		double[] result_ga = new double[repeat];
		double[] result_mimic = new double[repeat];

		double[] time_rhc = new double[repeat];
		double[] time_sa = new double[repeat];
		double[] time_ga = new double[repeat];
		double[] time_mimic = new double[repeat];

		for (int i = 0; i < repeat; i++) {
			double start = System.nanoTime(), end, trainingTime;

			RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
			FixedIterationTrainer fit = new FixedIterationTrainer(rhc, itr_rhc);
			fit.train();
			end = System.nanoTime();
			trainingTime = end - start;
			// System.out.println(ef.value(rhc.getOptimal()));
			result_rhc[i] = ef.value(rhc.getOptimal());
			time_rhc[i] = trainingTime;

			start = System.nanoTime();
			SimulatedAnnealing sa = new SimulatedAnnealing(100, .95, hcp);
			fit = new FixedIterationTrainer(sa, itr_sa);
			fit.train();
			end = System.nanoTime();
			trainingTime = end - start;
			// System.out.println(ef.value(sa.getOptimal()));
			result_sa[i] = ef.value(sa.getOptimal());
			time_sa[i] = trainingTime;

			start = System.nanoTime();
			StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 25, gap);
			fit = new FixedIterationTrainer(ga, itr_ga);
			fit.train();
			end = System.nanoTime();
			trainingTime = end - start;
			// System.out.println(ef.value(ga.getOptimal()));
			result_ga[i] = ef.value(ga.getOptimal());
			time_ga[i] = trainingTime;

			start = System.nanoTime();
			MIMIC mimic = new MIMIC(200, 100, pop);
			fit = new FixedIterationTrainer(mimic, itr_mimic);
			fit.train();
			end = System.nanoTime();
			trainingTime = end - start;
			// System.out.println(ef.value(mimic.getOptimal()));
			result_mimic[i] = ef.value(mimic.getOptimal());
			time_mimic[i] = trainingTime;
		}

		System.out.println(average(result_rhc));
		System.out.println(average(result_sa));
		System.out.println(average(result_ga));
		System.out.println(average(result_mimic));

		System.out.println(average(time_rhc));
		System.out.println(average(time_sa));
		System.out.println(average(time_ga));
		System.out.println(average(time_mimic));

		System.out.println("--------");
	}

	private static double average(double[] result) {
		double average = 0.0;
		for (int i = 0; i < result.length; i++) {
			average += result[i];
		}
		average /= result.length;
		return average;
	}
}
