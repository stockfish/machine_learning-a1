package randopt;

import java.util.Arrays;
import java.util.Random;

import dist.DiscreteDependencyTree;
import dist.DiscretePermutationDistribution;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import opt.SwapNeighbor;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.SwapMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * Travelling Salesman Problem: compare RHC, SA, GA and MIMIC
 * 
 * Modified by Xiaolu, original version is Andrew Guillory's CountOnesTest
 */
public class TravelingSalesmanTest {
	/** The n value */
	private static final int N = 30;

	/**
	 * The test main
	 * 
	 * @param args
	 *            ignored
	 */
	public static void main(String[] args) {
		Random random = new Random();
		// create the random points
		double[][] points = new double[N][2];
		for (int i = 0; i < points.length; i++) {
			points[i][0] = random.nextDouble();
			points[i][1] = random.nextDouble();
		}

		int repeat = 5;
		double[] result_rhc = new double[repeat];
		double[] result_sa = new double[repeat];
		double[] result_ga = new double[repeat];
		double[] result_mimic = new double[repeat];
		double[] time_rhc = new double[repeat];
		double[] time_sa = new double[repeat];
		double[] time_ga = new double[repeat];
		double[] time_mimic = new double[repeat];
		
		int iter = 2000;
		for (int i = 0; i < repeat; i++) {
			// for rhc, sa, and ga we use a permutation based encoding
			TravelingSalesmanEvaluationFunction ef = new TravelingSalesmanRouteEvaluationFunction(points);
			Distribution odd = new DiscretePermutationDistribution(N);
			NeighborFunction nf = new SwapNeighbor();
			MutationFunction mf = new SwapMutation();
			CrossoverFunction cf = new TravelingSalesmanCrossOver(ef);
			HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);
			GeneticAlgorithmProblem gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);

			double start = System.nanoTime(), end, trainingTime;
			
			
			RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
			FixedIterationTrainer fit = new FixedIterationTrainer(rhc, iter);
			
			fit.train();
			//System.out.println(ef.value(rhc.getOptimal()));
			end = System.nanoTime();
			trainingTime = end - start;
			trainingTime /= Math.pow(10, 9);
			//System.out.println(trainingTime);
			result_rhc[i] = ef.value(rhc.getOptimal());
			time_rhc[i] = trainingTime;

			start = System.nanoTime();
			SimulatedAnnealing sa = new SimulatedAnnealing(1E12, .95, hcp);
			fit = new FixedIterationTrainer(sa, iter);
			fit.train();
			//System.out.println(ef.value(sa.getOptimal()));
			end = System.nanoTime();
			trainingTime = end - start;
			trainingTime /= Math.pow(10, 9);
			//System.out.println(trainingTime);
			result_sa[i] = ef.value(sa.getOptimal());
			time_sa[i] = trainingTime;

			start = System.nanoTime();
			StandardGeneticAlgorithm ga = new StandardGeneticAlgorithm(200, 150, 20, gap);
			fit = new FixedIterationTrainer(ga, iter);
			fit.train();
			//System.out.println(ef.value(ga.getOptimal()));
			end = System.nanoTime();
			trainingTime = end - start;
			trainingTime /= Math.pow(10, 9);
			//System.out.println(trainingTime);
			result_ga[i] = ef.value(ga.getOptimal());
			time_ga[i] = trainingTime;
			
			// for mimic we use a sort encoding
			start = System.nanoTime();
			ef = new TravelingSalesmanSortEvaluationFunction(points);
			int[] ranges = new int[N];
			Arrays.fill(ranges, N);
			odd = new DiscreteUniformDistribution(ranges);
			Distribution df = new DiscreteDependencyTree(.1, ranges);
			ProbabilisticOptimizationProblem pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);

			MIMIC mimic = new MIMIC(200, 100, pop);
			fit = new FixedIterationTrainer(mimic, iter);
			fit.train();
			//System.out.println(ef.value(mimic.getOptimal()));
			end = System.nanoTime();
			trainingTime = end - start;
			trainingTime /= Math.pow(10, 9);
			//System.out.println(trainingTime);
			result_mimic[i] = ef.value(mimic.getOptimal());
			time_mimic[i] = trainingTime;
		}
		
		System.out.println("TSP");
		System.out.println(N);
		
		System.out.println(average(result_rhc));
		System.out.println(average(result_sa));
		System.out.println(average(result_ga));
		System.out.println(average(result_mimic));
		
		System.out.println(average(time_rhc));
		System.out.println(average(time_sa));
		System.out.println(average(time_ga));
		System.out.println(average(time_mimic));
		
		System.out.println(iter);

	}
	
	private static double average(double[] result) {
		double average = 0.0;
		for (int i = 0; i < result.length; i++) {
			average += result[i];
		}
		average /= result.length;
		return average;
	}
}
