package randopt;

import java.util.Arrays;

import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.*;
import opt.ga.CrossoverFunction;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.UniformCrossOver;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;

/**
 * Count Ones: for tuning SA's parameter
 * 
 * Modified by Xiaolu, original version is Andrew Guillory's CountOnesTest
 */
public class CountOnesSAParameter {
	/** The n value */
	private static final int N = 500;

	public static void main(String[] args) {
		int[] ranges = new int[N];
		Arrays.fill(ranges, 2);
		EvaluationFunction ef = new CountOnesEvaluationFunction();
		Distribution odd = new DiscreteUniformDistribution(ranges);
		NeighborFunction nf = new DiscreteChangeOneNeighbor(ranges);
		HillClimbingProblem hcp = new GenericHillClimbingProblem(ef, odd, nf);

		// Set iteration
		int iter = 2000;
		int repeat = 100;
		double[] result_rhc = new double[repeat];
		double[] result_sa = new double[repeat];

		double[] time_rhc = new double[repeat];
		double[] time_sa = new double[repeat];
		
		//for (double temp = 1e9; temp > 1e3; temp /= 10) {
		for (double cooling_rate = 0.95; cooling_rate > 0.05; cooling_rate -= 0.05) {
			for (int i = 0; i < repeat; i++) {
				double start = System.nanoTime(), end, trainingTime;

				RandomizedHillClimbing rhc = new RandomizedHillClimbing(hcp);
				FixedIterationTrainer fit = new FixedIterationTrainer(rhc, 10 * iter);
				fit.train();
				end = System.nanoTime();
				trainingTime = end - start;
				// System.out.println(ef.value(rhc.getOptimal()));
				result_rhc[i] = ef.value(rhc.getOptimal());
				time_rhc[i] = trainingTime;

				start = System.nanoTime();
				SimulatedAnnealing sa = new SimulatedAnnealing(1e9, cooling_rate, hcp);
				//SimulatedAnnealing sa = new SimulatedAnnealing(temp, 0.55, hcp);
				fit = new FixedIterationTrainer(sa, 10 * iter);
				fit.train();
				end = System.nanoTime();
				trainingTime = end - start;
				// System.out.println(ef.value(sa.getOptimal()));
				result_sa[i] = ef.value(sa.getOptimal());
				time_sa[i] = trainingTime;

			}
			System.out.println("-----");
			System.out.println(cooling_rate);
			//System.out.println(temp);
			System.out.println(average(result_rhc));
			System.out.println(average(result_sa));

			System.out.println(average(time_rhc));
			System.out.println(average(time_sa));

		}
	}

	public static double average(double[] result_list) {
		// 'average' is undefined if there are no elements in the list.
		// Calculate the summation of the elements in the list
		long sum = 0;
		int n = result_list.length;
		// Iterating manually is faster than using an enhanced for loop.
		for (int i = 0; i < n; i++)
			sum += result_list[i];
		// We don't want to perform an integer division, so the cast is
		// mandatory.
		return ((double) sum) / n;
	}
}
