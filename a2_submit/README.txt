all files can be found on bitbuck
https://bitbucket.org/stockfish/machine_learning-a1/src/master/

This file contains the instructions for how to run the code for Assignment 2

Environment:
This project uses the ABAGAIL package in Eclipse. The most recent version of ABAGAIL can be found at https://github.com/pushkar/ABAGAIL, and please follow the instructions of run ABAGAIL using Eclipse http://abagail.readthedocs.io/en/latest/faq.html#how-to-use-abagail-with-eclipse.

The python scripts can be run using Anaconda and Jupiter notebook (Python 2.7).

Dataset:
1) breast-cancer-w-diag.csv - The breast cancer Wisconsin (diagnostic) dataset
1) btrain - The training set
2) btest.csv - The testing set

Code Files:
1)split_train_test_4assgnment2.ipynb - Code for split training and test sets

In nnet folder:
2) RHC4BC.java - Code for Randomised Hill Climbing training of neural network
3) SA4BC.java - Code for Simulated Annealing training of neural network
4) GA4BC.java - Code for Genetic Algorithm training of neural network
5) BCTest.java - Code for Optimized parameters for 4 algos, as well as error.

In randopt folder:
6) TravelingSalesmanTest.java - Code to use Randomised Optimization to solve the Traveling Salesman Problem
7) CountOnesTest.java - Code to use Randomised Optimization to solve the Count Ones Problem
8) CountOnesSAParameter.java - Code for SA parameter tuning for Count Ones Problem
9) KnapsackTest.java - Code to use Randomised Optimization to solve the Knapsack Problem

10) KnapsackTotalTimeOptimized.java - Code for generate total time spend for 4 algorithms to solve Knapsack problem using optimized parameters.

Plotting and Results:
Results were copied from console and saved in Excel files. We use Excel to perform plotting.
